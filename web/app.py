from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
	name = request.args.get("name")
	if name == "name":
		return render_template("trivia.html", index)

	elif name == ".." or name == "//" or name == "~/":
		return errorhandler(404)
	else:
		return errorhandler(403)

@app.errorhandler(403)
def file_not_found():
	return render_template("403.html", 403)

@app.errorhandler(404)
def file_is_forbidden():
	return render_template("404.html", 404)


if __name__ == "__main__":
    app.run(debug=True,port=5000)



